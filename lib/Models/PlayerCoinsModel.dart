import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';

Future<PlayerCoinsModelClass> getPlayerCoins() async
{
  final response=await http.post(Uri.parse('$apiPath/api/PlayerCoins/GetPlayerCoins'),body:"{'PlayerCoinsGetter':{'AuthId':'$AuthId','UserName':'$UserName','Password':'$Password','FeetId':'$feetId'}}");
  if(response.statusCode==200)
    {
      return PlayerCoinsModelClass.fromJson(json.decode(response.body));
    }
  else
    {
      throw Exception();
    }
}
class PlayerCoinsModelClass {
  final int playerCoins;
  const PlayerCoinsModelClass({required this.playerCoins});
 factory PlayerCoinsModelClass.fromJson(dynamic json)
 {
   return PlayerCoinsModelClass(
     playerCoins: json['playerCoins']
   );
 }
}

class PlayerCoinsBuilder extends StatelessWidget {
  final int playerCoins;
  PlayerCoinsBuilder({required this.playerCoins});
  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children:[
        Center(
          child:Text.rich(
            TextSpan(
            children: [
              const WidgetSpan(child:Icon(Icons.donut_small_sharp, size:77.0, color: textColor),alignment: PlaceholderAlignment.middle),
              TextSpan(text:'-${NumberFormat.decimalPattern().format(playerCoins)}'),
              ]
          ))
        ),
        TextButton(
          onPressed:(){
            Navigator.pushReplacementNamed(context, '/FeetCoins');
          },
          child:const Text('Store',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset:Offset.zero
          )]))
        ),
        TextButton(
          onPressed:(){},
          child:const Text('Watch Ad',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset:Offset.zero
          )]))
        )
      ]
    );
  }
}
