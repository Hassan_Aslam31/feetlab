import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

Future<FriendProfileModelClass> getFriendProfile(int friendFeetId) async
{
  final response=await http.post(Uri.parse('$apiPath/api/PlayerFriends/GetFriendProfile'),body:"{'FeetFriendsGetter':{'AuthId':'$AuthId','UserName':'$UserName','Password':'$Password','FeetId':'0','FriendFeetId':'$friendFeetId'}}");
  if(response.statusCode==200)
    {
      return FriendProfileModelClass.fromJson(json.decode(response.body));
    }
  else
    {
      throw Exception();
    }
}
class FriendProfileModelClass
{
 final int friendFeetId;
 final String friendName;
 final String profileImage;
 final int runWon;
 final int runLost;
 final int chasedWon;
 final int chasedLost;
 final int friendCoins;
 const FriendProfileModelClass({required this.friendFeetId,required this.friendName,required this.profileImage,
   required this.runWon,required this.runLost,required this.chasedWon,required this.chasedLost,required this.friendCoins});
 factory FriendProfileModelClass.fromJson(dynamic json){
   return FriendProfileModelClass(
       friendFeetId:json['friendFeetId'],
       friendName:json['friendName'],
       profileImage:json['profileImage'],
       runWon:json['runWon'],
       runLost:json['runLost'],
       chasedWon:json['chasedWon'],
       chasedLost:json['chasedLost'],
       friendCoins:json['friendCoins']
   );
 }
}

class FriendProfileBuilder extends StatelessWidget {
  final FriendProfileModelClass friendObj;
  FriendProfileBuilder({required this.friendObj});
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
         Row(
           mainAxisAlignment: MainAxisAlignment.center,
             children:[ const Icon(Icons.supervised_user_circle_rounded, size:87.0, color: textColor),
           Text(friendObj.friendName,style:const TextStyle(fontSize: 27.0))
         ]),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children:[ const Icon(Icons.donut_small_sharp, size:77.0, color: textColor),
              Text(NumberFormat.decimalPattern().format(friendObj.friendCoins),style:const TextStyle(fontSize: 27.0))
            ]),
         DataTable(
           showBottomBorder: true,
             columns: const [
             DataColumn(label: Text('')),
             DataColumn(label: Text('Run',style: TextStyle(fontWeight: fontWeight,fontSize:21.0,color: textColor))),
             DataColumn(label: Text('Chased',style:TextStyle(fontWeight: fontWeight,fontSize:21.0,color: textColor)))
           ],
           rows: [
             DataRow(cells: [
               const DataCell(Text('Won',style: TextStyle(fontWeight: fontWeight,fontSize:23.0,color: textColor))),
               DataCell(Text('${friendObj.runWon}',style:const TextStyle(fontWeight: fontWeight,fontSize:21.0,color: textColor))),
                DataCell(Text('${friendObj.chasedWon}',style:const TextStyle(fontWeight: fontWeight,fontSize:21.0,color: textColor)))
             ]
             ),
             DataRow(cells: [
              const DataCell(Text('Lost',style: TextStyle(fontWeight: fontWeight,fontSize:23.0,color: textColor))),
               DataCell(Text('${friendObj.chasedWon}',style:const TextStyle(fontWeight: fontWeight,fontSize:21.0,color: textColor))),
               DataCell(Text('${friendObj.chasedLost}',style:const TextStyle(fontWeight: fontWeight,fontSize:21.0,color: textColor)))
             ]
             )
           ]
         ),
        TextButton(
          onPressed:(){
            Navigator.pushReplacementNamed(context, '/PlayingLevels',arguments: {'friendCoins':friendObj.friendCoins,'friendFeetId':friendObj.friendFeetId});
          },
          child:const Text('Challenge',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset:Offset.zero
          )]))
        )
      ]
    );
  }
}
