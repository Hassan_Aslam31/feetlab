import 'dart:core';
import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<List<ObjectiveModelClass>> getObjectiveList() async
{
  final response = await http.post(Uri.parse('$apiPath/api/FLUserObjectives/GetObjectives'),body:"{'ObjectivesGetter':{'AuthId':'$AuthId','UserName':'$UserName','Password':'$Password'}}");
  if (response.statusCode == 200) {
    final data=jsonDecode(response.body);
    return data.map<ObjectiveModelClass>((i)=>ObjectiveModelClass.fromJson(i)).toList();
  }
  else {
    throw Exception();
  }
}
class ObjectiveModelClass
{
  final int objectiveId;
  final String levelName;
  final String levelMeters;
  const ObjectiveModelClass({
    required this.objectiveId,
    required this.levelName,
    required this.levelMeters
  });
  factory ObjectiveModelClass.fromJson(dynamic json) {
    return ObjectiveModelClass(
        objectiveId: json['objectiveId'],
      levelName: json['levelName'],
      levelMeters: json['levelMeters']
    );
  }
}

class ObjectiveListBuilder extends StatelessWidget {
  final List<ObjectiveModelClass> objectivesList;
  ObjectiveListBuilder({required this.objectivesList});
  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      isAlwaysShown:true,
      showTrackOnHover: true,
      child: ListView.builder(
        itemCount: objectivesList.length,
        padding:const EdgeInsets.all(7.0),
        itemBuilder: (context,i){
          return InkWell(
            child: Card(
              child:Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(objectivesList[i].levelName,style:cardRowTextStyle),
                    Text(objectivesList[i].levelMeters,style:cardRowTextStyle),
                    const Icon(
                      Icons.arrow_forward_ios_rounded,
                      color:textColor
                    )
                  ]
                )
              )
            ),
              onTap: (){
                Navigator.pushReplacementNamed(context,'/PlayerStages',arguments: {'objectiveId':objectivesList[i].objectiveId,'levelName':objectivesList[i].levelName});
              }
          );
        }
      )
    );
  }
}



