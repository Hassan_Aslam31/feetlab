import 'dart:convert';
import 'package:device_info_plus/device_info_plus.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:feetlab/Views/layout.dart';

class userModelClass
{
 late var deviceData;
 late var devicePlugin;
 Future<Map<String, dynamic>> saveUserProfile(String deviceId,String deviceModel,String deviceManufacturer,String playerName,String dob,String timeZone,String utc,String playerDT) async {
  var uri=Uri.parse('$apiPath/User/UserOperation');
  var response=await http.post(uri,body:"{'FeetUserGetter':{'AuthId':'$AuthId','UserName':'$UserName','Password':'$Password','PlayerName':'$playerName','DOB':'$dob','DeviceId':'$deviceId','DeviceModel':'$deviceModel','DeviceManufacture':'$deviceManufacturer','TimeZone':'$timeZone','UTC':'$utc','PlayerDateTime':'$playerDT'}}");
  var responseJsonDecoded = json.decode(response.body);
  return responseJsonDecoded;
 }
 Future<Map<String, dynamic>> checkUserProfile(String deviceId) async {
  var uri=Uri.parse('$apiPath/User/UserExistCheck');
  var response=await http.post(uri,body:"{'FeetUserGetter':{'AuthId':'$AuthId','UserName':'$UserName','Password':'$Password','DeviceId':'$deviceId'}}");
  var responseJsonDecoded = json.decode(response.body);
  return responseJsonDecoded;
 }
 Future<dynamic> getDeviceInfo () async {
   devicePlugin=DeviceInfoPlugin();
  if(Platform.isAndroid) {
  deviceData=readAndroidBuildData(await devicePlugin.androidInfo);
  }
  else if(Platform.isIOS)
   {
    deviceData=readAndroidBuildData(await devicePlugin.iosInfo);
   }
  else
   {
    deviceData={'id':'unknown'};
   }
  return deviceData;
 }
 Map<String, dynamic> readAndroidBuildData(AndroidDeviceInfo build) {
  return {
   'manufacturer': build.manufacturer,
   'model': build.model,
   'id':build.id,
  };
 }
 Map<String, dynamic> readIosDeviceInfo(IosDeviceInfo data) {
  return {
   'model': data.utsname.machine,
   'id': data.identifierForVendor,
   'manufacturer':data.model,
  };
 }

}


