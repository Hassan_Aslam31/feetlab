import 'dart:core';
import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<List<PlayingLevelsModelClass>> getPLayingLevelsList() async
{
  final response=await http.post(Uri.parse('$apiPath/api/FLUserObjectives/GetPlayingLevels'),body:"{'ObjectivesGetter':{'AuthId':'$AuthId','UserName':'$UserName','Password':'$Password'}}");
  if(response.statusCode==200) {
    final data = json.decode(response.body);
    return data.map<PlayingLevelsModelClass>((i)=>PlayingLevelsModelClass.fromJson(i)).toList();
  }
  else
    {
      throw Exception();
    }
}
class PlayingLevelsModelClass
{
  final int levelId;
  final String partName;
  final String partLength;
  final int coins;
 const PlayingLevelsModelClass({required this.levelId,required this.partName,required this.partLength,required this.coins});
 factory PlayingLevelsModelClass.fromJson(dynamic json){
   return PlayingLevelsModelClass(
     levelId: json['levelId'],
     partName: json['partName'],
     partLength: json['partLength'],
     coins: json['coins']
   );
  }
}

class PLayingLevelsBuilder extends StatelessWidget {
  final List<PlayingLevelsModelClass> playingLevelsList;
  final int friendCoins;
  final int friendFeetId;
  PLayingLevelsBuilder({required this.playingLevelsList,required this.friendCoins,required this.friendFeetId});
  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      isAlwaysShown:true,
      showTrackOnHover: true,
      child:  GridView.count(
        padding:const EdgeInsets.all(7.0),
        crossAxisCount: 3,
        crossAxisSpacing: 3.0,
        mainAxisSpacing: 1.0,
        shrinkWrap: true,
        children: List.generate(playingLevelsList.length,(index) {
          return InkWell(
            onTap: () async {
                final int playingLevelCoins=playingLevelsList[index].coins;
                if(coinsGBL<playingLevelCoins)
                  {
                    await showCustomPopup(context,msg:"You do not have enough coins to Play this Level .");
                    Navigator.pushReplacementNamed(context, '/playercoins');
                  }
                  else if(friendCoins<playingLevelCoins)
                  {
                    await showCustomPopup(context,msg:"Your Friend do not have enough coins to Play this Level .");
                    Navigator.pushReplacementNamed(context, '/PlayerFriends');
                  }
                   else
                     {
                      await showPopupPlaying(context,friendFeetId.toString(),playingLevelCoins.toString(),playingLevelsList[index].partName,playingLevelsList[index].partLength);
                    }
            },
            child: Card(
              child:Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children:[
                   const Icon(Icons.play_arrow_rounded, size:23.0, color: textColor),
                    Text(playingLevelsList[index].partName,style:const TextStyle(fontSize: 21.0),textAlign: TextAlign.center),
                    Text(playingLevelsList[index].partLength,style:const TextStyle(fontSize: 21.0),textAlign: TextAlign.center),
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(text:'${playingLevelsList[index].coins}-',style:cardRowTextStyle),
                          const WidgetSpan(child:Icon(Icons.donut_small_sharp, size:21.0, color: textColor),alignment: PlaceholderAlignment.middle)
                        ]
                      )
                    )
                  ]
              )
            )
          );
        }
        ,growable: false)
      )
    );
  }
}
