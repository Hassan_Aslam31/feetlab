import 'dart:core';
import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<List<PlayerStagesModelClass>> getPlayerStagesList(int objectiveId) async
{
  final response = await http.post(Uri.parse('$apiPath/api/FLUserObjectives/GetPlayerStages'),body:"{'PlayerStageGetter':{'AuthId':'$AuthId','UserName':'$UserName','Password':'$Password','FeetId':'$feetId','ObjectiveId':'$objectiveId'}}");
  if (response.statusCode == 200) {
    final data=jsonDecode(response.body);
    return data.map<PlayerStagesModelClass>((i)=>PlayerStagesModelClass.fromJson(i)).toList();
  }
  else {
    throw Exception();
  }
}
class PlayerStagesModelClass {
  final String partName;
  final String partLength;
  final String status;
  final int coins;
  const PlayerStagesModelClass({required this.partName, required this.partLength, required this.status, required this.coins});
  factory PlayerStagesModelClass.fromJson(dynamic json){
    return PlayerStagesModelClass(
        partName: json['partName'],
        partLength: json['partLength'],
        status: json['status'],
        coins: json['coins']
    );
  }
}

class PlayerStagesListBuilder extends StatelessWidget {
  final List<PlayerStagesModelClass> stagesList;
  PlayerStagesListBuilder({required this.stagesList});
  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      isAlwaysShown:true,
      showTrackOnHover: true,
      child: ListView.builder(
        itemCount: stagesList.length,
        padding:const EdgeInsets.all(7.0),
        itemBuilder: (context,i){
          return Card(
            child:Padding(
              padding: const EdgeInsets.all(5.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(stagesList[i].partName,style:cardRowTextStyle),
                    Text(stagesList[i].partLength,style:cardRowTextStyle),
                    if(stagesList[i].status=='locked')
                     const Icon(
                         Icons.enhanced_encryption_rounded, color: textColor
                     )
                    else
                    const  Icon(
                         Icons.no_encryption_gmailerrorred, color: textColor
                     ),
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(text:'${stagesList[i].coins}-',style:cardRowTextStyle),
                          const WidgetSpan(child: Icon(Icons.donut_small_sharp),alignment: PlaceholderAlignment.middle)
                        ]
                      )
                    )
                  ]
                )
            )
          );
        }
      )
    );
  }
}
