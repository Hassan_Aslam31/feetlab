import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';



class SignUpModelClass
{
late int feetid;
late String username;
late String dob;
late String email;
late String phone;
SignUpModelClass({this.feetid=0,this.username="",this.dob="",this.email="",this.phone=""});
}
class SignUpModelBody extends StatelessWidget {
  late int dOBYearGBLCount;
  late String playerName;
  late String dobDay;
  late String dobMonth;
  late String dobYear;
  late TextEditingController _controller;
  late Map<String,String> validateDob;
  late String dobMonthNo;
  @override
  Widget build(BuildContext context) {
    dOBYearGBLCount=(DateTime.now().year-1900)+1;
    _controller=TextEditingController();
    dobDay="01";
    dobMonth="Jan";
    dobYear="1900";

    return Padding(
        padding:const EdgeInsets.all(13.0),
    child: Column(
    children:  [
    const Expanded(flex: 0,child:SizedBox(height:47.0)),
    Expanded(flex: 1, child:TextFormField(
      controller:_controller ,
            autocorrect: false,
            decoration:InputDecoration(
              counterStyle: const TextStyle(color: textColor),
              labelText: "Player Name",
              enabledBorder:  OutlineInputBorder(
                borderSide:const BorderSide(color: textColor),
                borderRadius: BorderRadius.circular(13.0),
              ),
             // filled: true,
              focusedBorder:OutlineInputBorder(
                borderSide:const BorderSide(color: appBarColor),
                borderRadius: BorderRadius.circular(13.0),
              ),
              floatingLabelStyle:const TextStyle(color: textColor),
              ),
      cursorColor: textColor,
      maxLength: 20,
      enableInteractiveSelection: false,
      //keyboardType: TextInputType.name,
      style:const TextStyle(color: textColor),
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp("[a-zA-Z ]")),
      ],
    ),
    ),

      const Expanded(flex: 0, child:Center(child: Text('Select your Date of Birth by the swipe of cards :',style: TextStyle(fontSize: 13.0),),)),
      Expanded(flex: 1, child:Row(
       //mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            flex:1,
            child: ListWheelScrollView.useDelegate(
              childDelegate: ListWheelChildBuilderDelegate(
                childCount: 31,
                builder: (context,i)=>DobDayCardBody(day: i),
              ),
                itemExtent: 77,
                diameterRatio: 0.7,
                physics:const FixedExtentScrollPhysics(),
                perspective: 0.010,
                offAxisFraction: 0,
                onSelectedItemChanged: (index){
                  dobDay=dayNameGBL[index];
                },
            ),
          ),
          Expanded(
            flex:1,
            child: ListWheelScrollView.useDelegate(itemExtent: 77,
                diameterRatio: 0.7,
                physics:const FixedExtentScrollPhysics(),
                perspective: 0.010,
                offAxisFraction: 0,
                childDelegate: ListWheelChildBuilderDelegate(
                  childCount: 12,
                  builder: (context,i)=>DobMonthCardBody(monthNo: i),
                ),
              onSelectedItemChanged: (index){
                dobMonth=monthNameGBL[index];
              },
            ),
          ),
          Expanded(
            flex:1,
            child: ListWheelScrollView.useDelegate(itemExtent: 77,
                diameterRatio: 0.7,
                physics:const FixedExtentScrollPhysics(),
                perspective: 0.010,
                offAxisFraction:0,
                childDelegate: ListWheelChildBuilderDelegate(
                  childCount: dOBYearGBLCount,
                  builder: (context,i)=>DobYearCardBody(year: i),
                ),
              onSelectedItemChanged: (index){
                dobYear=yearNameGBL[index];
              },
            ),
          ),
        ],
      )
      ),
      Expanded(flex: 1, child:Center(child:
      TextButton(
        onPressed:() async{
          playerName=_controller.text;
          dobMonthNo=(monthNameGBL.indexOf(dobMonth)+1)<10?"0${(monthNameGBL.indexOf(dobMonth)+1)}":(monthNameGBL.indexOf(dobMonth)+1).toString();
          validateDob=await validateDobGBL('$dobDay.$dobMonthNo.$dobYear');
          if(playerName.trim().isEmpty)
            {
              showCustomPopup(context,msg: "Please enter Player Name .");
            }
           else if(validateDob['msg']=="no")
            {
              showCustomPopup(context,msg: "Please select a Valid Date of Birth .");
            }
           else if(validateDob['msg']=="limit")
           {
            showCustomPopup(context,msg: "Player must be 12 years old , Your age is below 12 .");
           }
            else
              {
                  Navigator.pushReplacementNamed(context, '/user',arguments: {'playerName':playerName,'dob':validateDob['date'],'timeZone':validateDob['timeZone'],'utc':validateDob['utc'],'playerDT':validateDob['playerDateTime']});
              }
        },
        child:const Text('Submit',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
            color: Colors.grey,
            blurRadius: 10.0,
            offset:Offset.zero
        )])),
      ),
      )),
    ],
    ),
    );
  }
}

class DobMonthCardBody extends StatelessWidget {
  late int monthNo;
  final List<String> monthName=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  DobMonthCardBody({this.monthNo=0});
  @override
  Widget build(BuildContext context) {
    monthNameGBL=monthName.toList();
    return Card(
      color: cardColor,
      child:Text(monthName[monthNo]),
    );
  }
}
class DobDayCardBody extends StatelessWidget {
  late int day;
  final List<String> dayName=[];
  DobDayCardBody({this.day=0});
  @override
  Widget build(BuildContext context) {
    for (var i = 1; i <=31 ; i++) {
      dayName.add(i<10?"0"+i.toString():i.toString());
    }
    dayNameGBL=dayName.toList();
    return Card(
      color: cardColor,
      child:Text(dayName[day]),
    );
  }
}
class DobYearCardBody extends StatelessWidget {
  late int yearCurrnet;
  final int year;
  final List<String> yearName=[];
  DobYearCardBody({this.year=0});
  @override
  Widget build(BuildContext context) {
    yearCurrnet=DateTime.now().year;
    for (var i = 1900; i <=yearCurrnet ; i++) {
      yearName.add(i.toString());
    }
    yearNameGBL=yearName.toList();
    return Card(
      color: cardColor,
      child:Text(yearName[year]),
    );
  }
}







