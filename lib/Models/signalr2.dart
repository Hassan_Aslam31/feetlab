import 'dart:async';
import 'package:context_holder/context_holder.dart';
import 'package:signalr_flutter/signalr_flutter.dart';
import 'package:feetlab/Views/layout.dart';
//import 'package:uuid/uuid.dart';
class Signalr {
  String _signalRStatus = 'Unknown';
  late final SignalR signalR;

  Future<void> initSignal() async
  {
   // var uuid = const Uuid();
   // UUID=uuid.v4();
     signalR = SignalR(
        '$apiPath/signalr',
        "ChatHub",
        hubMethods: ["ListenChallenge"],
        queryString:'uuid=$feetId',
        statusChangeCallback: _onStatusChange,
        hubCallback: _onNewMessage);
        final isConnected=await signalR.isConnected??false;
        if(isConnected==false) {
          await signalR.connect();
        }
  }
 Future retryUntilSuccessfulConnection() async
  {
   var isco;
    do
    {
      await Future.delayed(const Duration(seconds: 4));
      try {
        await signalR.connect();
        isco=(await signalR.isConnected) as Future<bool>;
      }
      catch(exception) {
        showSnack(exception.toString());
      }
    }
    while(isco==true);
  }
  _onStatusChange(dynamic status) async{
    _signalRStatus = status as String;
    showSnack(_signalRStatus);
    if(_signalRStatus=="Disconnected")
    {
      await retryUntilSuccessfulConnection();
    }
  }
  _onNewMessage(String? methodName, dynamic message) {
    showSnack('MethodName = $methodName, Message = $message');
    if(methodName=="ListenChallenge")
      {
        ListenChallenge(message);
      }
  }

  Future<String> sendChallenge(String freindFeetId,String msg,String coins,String role) async {
    var res =await signalR.invokeMethod("SendChallenge",
        arguments:[freindFeetId,msg,coins,role]).catchError((error) {
      showSnack(error.toString());
    });
    return res.toString();
  }
  void ListenChallenge(dynamic arg)
   {
     showPopupChallengeAccept(ContextHolder.currentContext,arg['msg'],arg['coin'],arg['role'],arg['freindFeetId']);
   }
  Future<String> letsChallenge(String freindFeetId,String coins,String role) async {
    var res =await signalR.invokeMethod("LetsPlay",
        arguments: [freindFeetId,coins,role]).catchError((error) {
      showSnack(error.toString());
    });
    return res.toString();
  }

}
