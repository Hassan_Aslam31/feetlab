import 'dart:convert';
import 'package:feetlab/Views/layout.dart';
//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:qr_flutter/qr_flutter.dart';
//import 'package:qr_code_scanner/qr_code_scanner.dart';
//import 'dart:io';
class AddFreindsModel
{
  Future<Map<String, dynamic>> setPlayerFreinds(String friendFeetId) async{
      var uri = Uri.parse('$apiPath/PlayerFriends/SetPlayerFriends');
      var response = await http.post(uri,
          body: "{'FeetFriendsGetter':{'AuthId':'$AuthId','UserName':'$UserName','Password':'$Password','FeetId':'$feetId','FriendFeetId':'$friendFeetId'}}");
      var responseJsonDecoded = json.decode(response.body);
      return responseJsonDecoded;
      }
}
class AddFriendsQRCodeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(7.0),
      child: Column(
      //  mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            flex: 2,
            child:Center(child: QrImage(
              backgroundColor: Colors.white,
              data:'313hassanrazaaslam-${feetId.toString()}',
              version: QrVersions.auto,
              size: 200.0,
            )),
          ),
          const Expanded(
            flex: 1,
            child:Text("Your Friend Scans QR Code .",textAlign: TextAlign.center, style: TextStyle(fontSize: 21.0)),
          ),
        ],
      ),
    );
  }
}


