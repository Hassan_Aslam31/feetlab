import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

Future<List<FeetCoinsModelClass>> getFeetCoins() async
{
  final response=await http.post(Uri.parse('$apiPath/api/PlayerCoins/GetCoins'),body:"{'FeetCoinsGetter':{'AuthId':'$AuthId','UserName':'$UserName','Password':'$Password'}}");
  if(response.statusCode==200)
    {
      final data=json.decode(response.body);
      return data.map<FeetCoinsModelClass>((i)=>FeetCoinsModelClass.fromJson(i)).toList();
    }
  else
    {
      throw Exception();
    }
}
class FeetCoinsModelClass {
  final int feetCoinId;
  final int coins;
  final String price;
  const FeetCoinsModelClass({required this.feetCoinId,required this.coins,required this.price});
  factory FeetCoinsModelClass.fromJson(dynamic json)
  {
         return FeetCoinsModelClass(
          feetCoinId: json['feetCoinId'],
          coins: json['coins'],
          price: json['price']
        );
  }
}

class FeetCoinsListBuilder extends StatelessWidget {
   final List<FeetCoinsModelClass> coinsList;
   FeetCoinsListBuilder({required this.coinsList});
  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      isAlwaysShown:true,
      showTrackOnHover: true,
      child: ListView.builder(
        itemCount: coinsList.length,
        padding:const EdgeInsets.all(7.0),
        itemBuilder: (context,i){
          return InkWell(
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text.rich(
                        TextSpan(
                            children: [
                              TextSpan(text:'${coinsList[i].coins}-',style:cardRowTextStyle),
                              const WidgetSpan(child: Icon(Icons.donut_small_sharp),alignment: PlaceholderAlignment.middle)
                            ]
                        )
                    ),
                    Text.rich(
                        TextSpan(
                            children: [
                              TextSpan(text:'${coinsList[i].price}-',style:cardRowTextStyle),
                              const WidgetSpan(child: Icon(Icons.money_sharp),alignment: PlaceholderAlignment.middle)
                            ]
                        )
                    ),
                   const Icon(Icons.keyboard_arrow_down_sharp)
                  ]
                )
              )
            ),
              onTap: (){}
          );
        }
      )
    );
  }
}



