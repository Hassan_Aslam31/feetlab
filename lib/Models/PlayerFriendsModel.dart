import 'dart:convert';
import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<List<PlayerFriendsModelClass>> getPlayerFriendsList() async
{
  final response=await http.post(Uri.parse('$apiPath/api/PlayerFriends/GetPlayerFriends'),body:"{'FeetFriendsGetter':{'AuthId':'$AuthId','UserName':'$UserName','Password':'$Password','FeetId':'$feetId','FriendFeetId':'0'}}");
  if(response.statusCode==200)
    {
      final data=json.decode(response.body);
      return data.map<PlayerFriendsModelClass>((i)=>PlayerFriendsModelClass.fromJson(i)).toList();
    }
  else
    {
        throw Exception();
    }
}
class PlayerFriendsModelClass
{
  final String playerName;
  final String profileImage;
  final int friendFeetId;
  const PlayerFriendsModelClass({required this.playerName,required this.profileImage,required this.friendFeetId});
  factory PlayerFriendsModelClass.fromJson(dynamic json)
  {
   return PlayerFriendsModelClass(
       playerName: json['playerName'],
       profileImage: json['profileImage'],
       friendFeetId: json['friendFeetId']
   );
  }
}

class PlayerFriendsListBuilder extends StatelessWidget
{
  final List<PlayerFriendsModelClass> friendsList;
  PlayerFriendsListBuilder({required this.friendsList});
  @override
  Widget build(BuildContext context) {
   return Scrollbar(
     isAlwaysShown: true,
     showTrackOnHover: true,
     child: GridView.count(
       padding:const EdgeInsets.all(7.0),
          crossAxisCount: 3,
          crossAxisSpacing: 3.0,
          mainAxisSpacing: 1.0,
          children: List.generate(friendsList.length, (index) {
           return FriendsCardView(obj: friendsList[index]);
          }
      )
     )
   );
  }
}
class FriendsCardView extends StatelessWidget
{
  final PlayerFriendsModelClass obj;
  FriendsCardView({required this.obj});
  @override
  Widget build(BuildContext context) {
    return Card(
        child: InkWell(
          onTap: (){
            Navigator.pushReplacementNamed(context, '/friendProfile',arguments: {'friendFeetId':obj.friendFeetId});
          },
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children:[
           const Icon(Icons.supervised_user_circle_rounded),
            Text(obj.playerName),
             const Icon(Icons.arrow_forward_rounded)
                   ]
          )
         )
     );
  }
}
