import 'package:feetlab/Views/PlayerStages.dart';
import 'package:feetlab/Views/safearea.dart';
import 'package:feetlab/Views/user.dart';
import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';
import 'package:feetlab/Views/MainScreen.dart';
import 'package:feetlab/Views/Objectives.dart';
import 'package:flutter/services.dart';
import 'package:feetlab/Views/FeetCoins.dart';
import 'package:feetlab/Views/PlayerCoins.dart';
import 'package:feetlab/Views/signup.dart';
import 'package:sizer/sizer.dart';
import 'package:feetlab/Views/beaware.dart';
import 'package:feetlab/Views/children.dart';
import 'package:feetlab/Views/knownperson.dart';
import 'package:feetlab/Views/PlayerFriends.dart';
import 'package:feetlab/Views/addfriends.dart';
import 'package:feetlab/Views/FriendProfile.dart';
import 'package:feetlab/Views/PlayingLevels.dart';
import 'package:feetlab/Views/PlayWaiting.dart';
import 'package:context_holder/context_holder.dart';
import 'package:feetlab/Views/play.dart';
void main()  {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: appBarColor,
    systemNavigationBarColor: appBarColor,
   statusBarBrightness: Brightness.dark,
    statusBarIconBrightness: Brightness.dark,
    systemNavigationBarIconBrightness: Brightness.dark
  ));
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(
      Sizer(
      builder: (context, orientation, deviceType) {
        SizerUtil.deviceType== DeviceType.mobile?SizedBox(   // Widget for Mobile
          width: 100.w,
          height: 20.5.h
        )
            : SizedBox(   // Widget for Tablet
          width: 100.w,
          height: 12.5.h
        );
     return MaterialApp(
       navigatorKey: ContextHolder.key,
      routes: {
        '/': (context)=>PlayWaiting(),
        '/MainScreen':(context)=>MainScreen(),
        '/Objectives':(context)=>Objectives(),
        '/PlayerStages':(context)=>PlayerStages(),
        '/PlayerCoins':(context)=>PlayerCoins(),
        '/FeetCoins':(context)=>FeetCoins(),
        '/signup':(context)=>signUp(),
        '/user':(context)=>userInfo(),
        '/beawareMsg':(context)=>beawareMsg(),
        '/childrenMsg':(context)=>childrenMsg(),
        '/safeAreaMsg':(context)=>safeAreaMsg(),
        '/knownPerson':(context)=>knownPersonMsg(),
        '/PlayerFriends':(context)=>PlayerFriends(),
        '/addFriend':(context)=>addFriends(),
        '/friendProfile':(context)=>FriendProfile(),
        '/PlayingLevels':(context)=>PlayingLevels(),
        //'/PlayWaiting':(context)=>,
        '/play':(context)=>GamePlay()
      },
    debugShowCheckedModeBanner: false,
        theme: ThemeData(
          scaffoldBackgroundColor:themeColor,
          fontFamily: fontFamily,
          textTheme:const TextTheme(bodyText1: TextStyle(color:textColor,fontWeight:fontWeight,fontSize: fontSizeApp ),bodyText2:TextStyle(color:textColor,fontWeight:fontWeight,fontSize: fontSizeApp) ),
          appBarTheme:const AppBarTheme(backgroundColor: appBarColor,titleTextStyle: TextStyle(color:textColor,fontFamily: fontFamily, fontWeight:
          fontWeight,
            fontSize: appBarFontSize,
          )),
          floatingActionButtonTheme:const FloatingActionButtonThemeData(
            backgroundColor: appBarColor,
          ),
          cardTheme:CardTheme(
            color: cardColor,
          )
        ),
  );
      }
      )
  );
}





