import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';

class knownPersonMsg extends StatefulWidget {
  const knownPersonMsg({Key? key}) : super(key: key);

  @override
  _knownPersonMsgState createState() => _knownPersonMsgState();
}

class _knownPersonMsgState extends State<knownPersonMsg> {
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context,'/safeAreaMsg');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          body:Padding(
            padding: const EdgeInsets.all(13.0),
            child: Column(
              crossAxisAlignment:CrossAxisAlignment.stretch,
              children: [
                const Expanded(
                  flex: 1,
                  child: Icon(
                   // Icons.supervised_user_circle_rounded,
                    Icons.connect_without_contact_rounded,
                    size: 77.0,

                  ),
                ),
                const Expanded(
                  flex: 1,
                  child: Center(
                    child: Text(
                      'FeetLab should be played with known friends .',
                      style: TextStyle(fontSize: 21.0),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Center(
                    child: TextButton(
                      onPressed:(){
                        Navigator.pushReplacementNamed(context,'/signup');
                      },
                      child:const Text('Ok',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
                          color: Colors.grey,
                          blurRadius: 10.0,
                          offset:Offset.zero
                      )])),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
