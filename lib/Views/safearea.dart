import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';

class safeAreaMsg extends StatefulWidget {
  const safeAreaMsg({Key? key}) : super(key: key);

  @override
  _safeAreaMsgState createState() => _safeAreaMsgState();
}

class _safeAreaMsgState extends State<safeAreaMsg> {
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context,'/childrenMsg');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          body:Padding(
            padding: const EdgeInsets.all(13.0),
            child: Column(
              crossAxisAlignment:CrossAxisAlignment.stretch,
              children: [
                const Expanded(
                  flex: 1,
                  child: Icon(
                    Icons.do_not_step_rounded,
                    size: 77.0,

                  ),
                ),
                const Expanded(
                  flex: 1,
                  child: Center(
                    child: Text(
                      'Safe Area is strongly recommended while playing FeetLab .',
                      style: TextStyle(fontSize: 21.0),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Center(
                    child: TextButton(
                      onPressed:(){
                        Navigator.pushReplacementNamed(context,'/knownPerson');
                      },
                      child:const Text('Ok',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
                          color: Colors.grey,
                          blurRadius: 10.0,
                          offset:Offset.zero
                      )])),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
