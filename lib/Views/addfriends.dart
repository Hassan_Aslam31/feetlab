import 'package:feetlab/Models/addfriendsmodel.dart';
import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'dart:io';
class addFriends extends StatefulWidget {
  @override
  _addFriendsState createState() => _addFriendsState();
}

class _addFriendsState extends State<addFriends> {
  @override
  void initState() {
    super.initState();
    mainLoader = mainLoaderBack;
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      setState(() {
        mainLoader = AddFriendsQRCodeBody();
      });
    });
  }

  // @override
  // void dispose() {
  //   super.dispose();
  // }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context, '/playerFriends');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Add Friend"),
            leading: TextButton.icon(
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/playerFriends');
              },
              label: const Text(''),
              icon: const Icon(Icons.arrow_back_rounded, color: textColor),
            ),
            titleSpacing: 0,
          ),
          // body:screens[currentBottomIndex],
          body: mainLoader,
          bottomNavigationBar: CurvedNavigationBar(
              index: 0,
              onTap: (ind) {
                setState(() {
                  // currentBottomIndex=ind;
                  // mainLoader=screens[currentBottomIndex];
                  if (ind == 0) {
                    mainLoader = mainLoaderBack;
                    mainLoader = AddFriendsQRCodeBody();
                  }
                  else {
                    mainLoader = mainLoaderBack;
                    mainLoader = AddFriendsQRScanBody(context);
                  }
                });
              },
              buttonBackgroundColor: appBarColor,
              color: appBarColor,
              backgroundColor: themeColor,
              animationCurve: Curves.ease,
              height: 49.0,
              items: const [
                Icon(
                  Icons.qr_code_2,
                  color: textColor,
                  size: 41.0,
                ),
                Icon(
                  Icons.qr_code_scanner_sharp,
                  color: textColor,
                  size: 41.0,
                )
              ]
          ),
        ),
      ),
    );
  }
  Barcode?  result;
  QRViewController? controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  // late Widget currentPage=mainLoader;
  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    }
    controller!.resumeCamera();
  }

  Widget AddFriendsQRScanBody(context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        _buildQrView(context),
        Positioned (
          bottom: MediaQuery.of(context).size.width * 0.1,
          child:const Center(child: Text("Scan your Friend QR Code .", style: TextStyle(fontSize: 21.0,color:appBarColor))),
        ),
        Positioned(
          top: MediaQuery.of(context).size.width * 0.1,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(primary: appBarColor,textStyle:const TextStyle(fontFamily: fontFamily,fontWeight: fontWeight,fontSize: 17.0),onPrimary: textColor),
            onPressed: () async {
              await controller?.toggleFlash();
             // setState(() {});
            },
            child: FutureBuilder(
              future: controller?.getFlashStatus(),
              builder: (context, snapshot) {
                return Text('Flash ${snapshot.data==true?'Off':'On'}');
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = MediaQuery.of(context).size.width * 0.8;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor:appBarColor,
          borderLength: 13,
          borderWidth: 5,
          cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onQRViewCreated(QRViewController controller){
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) async {
      result =scanData;
      if(result!.code!.contains('313hassanrazaaslam')) {
        setState((){
          mainLoader = mainLoaderBack;
        });
        showSnack("Scanned successfully.");
        var splitCode=result!.code!.split('-');
        final obj=AddFreindsModel();
        try
        {
          var response = await obj.setPlayerFreinds(splitCode[1]);
          if(int.parse(response['responseCode'])==200)
          {
            Navigator.pushReplacementNamed(context, '/friendProfile',arguments: {'frndFeetId':splitCode[1]});
          }
          else
          {
           // showSnack(response.toString());
            setState(() {
              mainLoader = mainServiceMSG;
            });
          }
        }
        catch(e)
        {
          //showSnack(e.toString());
          setState(() {
            mainLoader = mainServiceMSG;
          });
        }
      }
    });
  }
  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    if (!p) {
      showSnack('Camera Access Permission is denied .');
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
