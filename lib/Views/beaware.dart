import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';
class beawareMsg extends StatefulWidget {
  const beawareMsg({Key? key}) : super(key: key);

  @override
  _beawareMsgState createState() => _beawareMsgState();
}

class _beawareMsgState extends State<beawareMsg> {
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return showExitPopup(context);
      },
      child: SafeArea(
        child: Scaffold(
            body:Padding(
              padding: const EdgeInsets.all(13.0),
              child: Column(
                crossAxisAlignment:CrossAxisAlignment.stretch,
                children: [
                  const Expanded(
                    flex: 1,
                    child: Icon(
                      Icons.warning_rounded,
                      size: 77.0,

                    ),
                  ),
                  const Expanded(
                    flex: 1,
                    child: Center(
                      child: Text(
                        'Be Aware of your surroundings while playing FeetLab .',
                        style: TextStyle(fontSize: 21.0,),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: TextButton(
                        onPressed:(){
                          Navigator.pushReplacementNamed(context, '/childrenMsg');
                        },
                        child:const Text('Ok',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
                            color: Colors.grey,
                            blurRadius: 10.0,
                            offset:Offset.zero
                        )])),
                      ),
                    ),
                  )
                ],
              ),
            ),
        ),
      ),
    );
  }
}
