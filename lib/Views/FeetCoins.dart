import 'package:flutter/material.dart';
import 'package:feetlab/Models/FeetCoinsModel.dart';
import 'package:feetlab/Views/layout.dart';

class FeetCoins extends StatefulWidget {
  @override
  _FeetCoinsState createState() => _FeetCoinsState();
}
class _FeetCoinsState extends State<FeetCoins> {
late Future<List<FeetCoinsModelClass>> futureData;

  @override
  void initState()
  {
    super.initState();
    futureData=getFeetCoins();
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:() async{
        Navigator.pushReplacementNamed(context, '/PlayerCoins');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title:const Text('Coins Store'),
              leading: TextButton.icon(
                onPressed: (){
                  Navigator.pushReplacementNamed(context, '/PlayerCoins');
                },
                label:const Text(''),
                icon:const Icon(Icons.arrow_back_rounded,color:textColor),
              ),
              titleSpacing:0
            ),
            body:FutureBuilder<List<FeetCoinsModelClass>>(
              future: futureData,
              builder: (context,snapshot){
                if(snapshot.hasData)
                  {
                    return FeetCoinsListBuilder(coinsList:snapshot.data!);
                  } else if(snapshot.hasError)
                    {
                      return mainServiceMSG;
                    }
                return mainLoader;
              }
            )
        )
      )
    );
  }
}




