import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'package:feetlab/Models/PlayerFriendsModel.dart';

class PlayerFriends extends StatefulWidget {
  @override
  _PlayerFriendsState createState() => _PlayerFriendsState();
}
class _PlayerFriendsState extends State<PlayerFriends> {
  late Future<List<PlayerFriendsModelClass>> futureData;

  @override
  void initState() {
    super.initState();
    futureData=getPlayerFriendsList();
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context, '/MainScreen');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: const Text("friends"),
              leading: TextButton.icon(
                onPressed: () {
                  Navigator.pushReplacementNamed(context, '/MainScreen');
                },
                label: const Text(''),
                icon: const Icon(Icons.arrow_back_rounded, color: textColor),
              ),
              titleSpacing: 0,
            ),
            body: FutureBuilder<List<PlayerFriendsModelClass>>(
              future: futureData,
              builder: (context,snapshot) {
                if(snapshot.hasData)
                  {
                    return PlayerFriendsListBuilder(friendsList:snapshot.data!);
                  } else if(snapshot.hasError)
                  {
                    return mainServiceMSG;
                  }
                  return mainLoader;
              }
            ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/addFriend');
              },
            child: const Icon(
              Icons.add,
              color: textColor,
              size: 27.0
            )
          )
        )
      )
    );
  }
}