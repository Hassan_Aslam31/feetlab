import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'package:feetlab/Models/PlayingLevelsModel.dart';

class PlayingLevels extends StatefulWidget {
  @override
  _PlayingLevelsState createState() => _PlayingLevelsState();
}
class _PlayingLevelsState extends State<PlayingLevels> {
 late Future<List<PlayingLevelsModelClass>> futureData;
 late dynamic routedValue;

 @override
  void initState() {
    super.initState();
  }
  @override
  void didChangeDependencies() {
    routedValue=ModalRoute.of(context)?.settings.arguments;
    futureData=getPLayingLevelsList();
    super.didChangeDependencies();
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context, '/PlayerFriends');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: const Text("playing levels"),
            leading: TextButton.icon(
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/PlayerFriends');
              },
              label: const Text(''),
              icon: const Icon(Icons.arrow_back_rounded, color: textColor),
            ),
            titleSpacing: 0,
          ),
          body:FutureBuilder<List<PlayingLevelsModelClass>>(
            future:futureData ,
            builder: (context,snapshot)
            {
              if(snapshot.hasData)
                {
                  return PLayingLevelsBuilder(playingLevelsList: snapshot.data!,friendCoins: routedValue['friendCoins'],friendFeetId:routedValue['friendFeetId']);
                } else if(snapshot.hasError)
                {
                  return mainServiceMSG;
                }
              return mainLoader;
            }
          )
        )
      )
    );
  }
}