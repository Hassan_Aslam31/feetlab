import 'package:flutter/material.dart';
import 'package:feetlab/Models/ObjectiveModel.dart';
import 'package:feetlab/Views/layout.dart';

class Objectives extends StatefulWidget {
  @override
  _ObjectivesState createState() => _ObjectivesState();
}
class _ObjectivesState extends State<Objectives> {
  late Future<List<ObjectiveModelClass>> futureData;

  @override
  void initState()
  {
     super.initState();
     futureData = getObjectiveList();
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:() async{
        Navigator.pushReplacementNamed(context, '/MainScreen');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: const Text('objectives'),
           leading: TextButton.icon(
             onPressed: (){
               Navigator.pushReplacementNamed(context, '/MainScreen');
             },
             label:const Text(''),
             icon:const Icon(Icons.arrow_back_rounded,color:textColor)
           ),
              titleSpacing:0
          ),
            body:Center(
              child: FutureBuilder<List<ObjectiveModelClass>>(
              future:futureData,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ObjectiveListBuilder(objectivesList:snapshot.data!);
                  } else if (snapshot.hasError) {
                    return mainServiceMSG;
                  }
                  return mainLoader;
                }
              )
            )
        )
      )
    );
  }
}



