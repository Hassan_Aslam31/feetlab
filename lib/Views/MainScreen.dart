import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';
import 'package:feetlab/Models/signalr2.dart';
import 'package:permission_handler/permission_handler.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}
class _MainScreenState extends State<MainScreen> {
  //final signalrObj=Signalr();
  //final locationPermission=Permission.location;
  @override
  void initState() {
    super.initState();
     // signalrObj.initSignal();
     // signalrObjGBL=signalrObj;
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          titleSpacing: 0,
          title:const Text('FeetLab'),
          leading:const Image(
            image:AssetImage(footPrintImage),
            height: 10.0,
            width: 10.0,
          ),
          actions:[
            IconButton(
              icon:const Icon(Icons.account_circle_outlined),
                onPressed: (){},
              color:textColor,
              iconSize: 37.0
            ),
          ],
          // elevation: 0,
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                TextButton(
                onPressed:(){
                  Navigator.pushReplacementNamed(context, '/PlayerCoins');
                },
                child:const Text('Coins',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    offset:Offset.zero
                )]))
                    ),
                    TextButton(
                onPressed:(){
                  Navigator.pushReplacementNamed(context, '/PlayerFriends');
                },
                child:const Text('friends',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    offset:Offset.zero
                )]))
                    ),
                    TextButton(
                onPressed:(){
                    Navigator.pushReplacementNamed(context, '/Objectives');
                },
                child:const Text('Objectives',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    offset:Offset.zero
                )]))
                    ),
                  TextButton(
                      onPressed:(){},
                      child:const Text('how to play',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
                          color: Colors.grey,
                          blurRadius: 10.0,
                          offset:Offset.zero
                      )]))
                  ),
                    TextButton(
                onPressed:(){},
                child:const Text('Privacy Policy',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    offset:Offset.zero
                )]))
                    ),
                    TextButton(
                onPressed:(){},
                child:const Text('Terms & Conditions',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
                    color: Colors.grey,
                    blurRadius: 10.0,
                    offset:Offset.zero
                )]))
                    )
              ]
            )
          ]
        ),
        floatingActionButton:Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
               mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  FloatingActionButton(onPressed: (){},child:const Icon(Icons.settings_sharp))
                ]
            )
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked
        )
      );
  }
}
