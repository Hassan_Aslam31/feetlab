import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';

class PlayWaiting extends StatefulWidget {
  @override
  State<PlayWaiting> createState() => _PlayWaitingState();
}
class _PlayWaitingState extends State<PlayWaiting> {
  @override
  void initState()
  {
    super.initState();
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context,'/MainScreen');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Challenged"),
            leading: TextButton.icon(
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/MainScreen');
              },
              label: const Text(''),
              icon: const Icon(Icons.arrow_back_rounded, color: textColor),
            ),
            titleSpacing: 0,
          ),
          body:Padding(
            padding: const EdgeInsets.all(13.0),
            child: Column(
              crossAxisAlignment:CrossAxisAlignment.stretch,
              children: const [
                Expanded(
                  flex: 1,
                  child: Center(
                    child: Text(
                      "Your challenge has been sent to your friend. Please wait for play game until your friend accept or decline challenge .",
                      textAlign: TextAlign.center,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
