import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';

class childrenMsg extends StatefulWidget {
  const childrenMsg({Key? key}) : super(key: key);

  @override
  _childrenMsgState createState() => _childrenMsgState();
}

class _childrenMsgState extends State<childrenMsg> {
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context,'/beawareMsg');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          body:Padding(
            padding: const EdgeInsets.all(13.0),
            child: Column(
              crossAxisAlignment:CrossAxisAlignment.stretch,
              children: [
                const Expanded(
                  flex: 1,
                  child: Icon(
                    Icons.escalator_warning_rounded,
                    size: 77.0,

                  ),
                ),
                const Expanded(
                  flex: 1,
                  child: Center(
                    child: Text(
                      'Children must be supervised by their adults while playing FeetLab .',
                      style: TextStyle(fontSize: 21.0),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Center(
                    child: TextButton(
                      onPressed:(){
                        Navigator.pushReplacementNamed(context, '/safeAreaMsg');
                      },
                      child:const Text('Ok',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
                          color: Colors.grey,
                          blurRadius: 10.0,
                          offset:Offset.zero
                      )])),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

