import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';
import 'package:feetlab/Models/FriendProfileModel.dart';

class FriendProfile extends StatefulWidget {
  @override
  _FriendProfileState createState() => _FriendProfileState();
}
class _FriendProfileState extends State<FriendProfile> {
  late Future<FriendProfileModelClass> futureData;
  late dynamic routedValue;

  @override
  void initState() {
    super.initState();
  }
  @override
  void didChangeDependencies() {
    routedValue=ModalRoute.of(context)?.settings.arguments;
    futureData= getFriendProfile(routedValue['friendFeetId']);
    super.didChangeDependencies();
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacementNamed(context, '/PlayerFriends');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: const Text("friend profile"),
            leading: TextButton.icon(
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/PlayerFriends');
              },
              label: const Text(''),
              icon: const Icon(Icons.arrow_back_rounded, color: textColor),
            ),
            titleSpacing: 0
          ),
          body:FutureBuilder<FriendProfileModelClass>(
            future:futureData,
            builder: (context,snapshot){
              if(snapshot.hasData)
                {
                  return FriendProfileBuilder(friendObj:snapshot.data!);
                } else if(snapshot.hasError)
                {
                  return mainServiceMSG;
                }
              return mainLoader;
            }
          )
          )
        )
      );
  }
}
