import 'package:feetlab/Models/signupmodel.dart';
import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';

class signUp extends StatefulWidget {
  @override
  _signUpState createState() => _signUpState();
}
class _signUpState extends State<signUp> {
  void SetSignUpBody()
  {
    setState((){
      mainLoader=SignUpModelBody();
    });
  }
  @override
  void initState()
  {
    super.initState();
    mainLoader=mainLoaderBack;
    SetSignUpBody();
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        var focusNode=FocusScope.of(context);
        if (!focusNode.hasPrimaryFocus) {
          focusNode.unfocus();
        }
      },
      child: WillPopScope(
        onWillPop: () async {
          Navigator.pushReplacementNamed(context, '/knownPerson');
         return true;
        },
        child: SafeArea(
          child: Scaffold(
            body:mainLoader
            ),
          ),
      ),
    );
  }
}
