import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
//import 'package:feetlab/Models/signalr2.dart';
import 'package:fluttertoast/fluttertoast.dart';
const themeColor=Color(0xFFFDD600);
const appBarColor=Color(0xFFFFF052);
const textColor=Colors.brown;
var cardColor=Colors.yellow[300];
const fontWeight=FontWeight.bold;
const listTileFontSize=20.0;
const footPrintImage='assets/images/footprints.png';
const fontFamily='IndieFlower';
const appBarFontSize= 27.0;
const fontSizeApp=30.0;
const apiPath='http://localhost:50816';
const AuthId='3130000';
const UserName='HassanRazaAslam';
const Password='hassanaslam31';
late String UUID='';
late Widget mainLoader=const Center(
child:Text('Loading.......')
);
late Widget mainLoaderBack=const Center(
  child:Text('Loading.......')
);
late int feetId=50;
late int coinsGBL=0;
late Widget mainServiceMSG=const Center(
  child:Text('Service is not available .')
);

const cardRowTextStyle=TextStyle(
  fontSize: listTileFontSize,
  fontWeight: fontWeight,
  color: textColor,
);

Future<bool> showExitPopup(context) async {
  return await showDialog( //show confirm dialogue
    //the return value will be from "Yes" or "No" options
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: appBarColor,
      title:const Text('Exit App !',style: TextStyle(color: textColor)),
      content:const Text('Do you really want to exit ?',style: TextStyle(color: textColor)),
      actions:[
        TextButton(
          onPressed: () => Navigator.of(context).pop(false),
          //return false when click on "NO"
          child:const Text('No',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset:Offset.zero
          )])),
        ),

        TextButton(
          onPressed: () => Navigator.of(context).pop(true),
          //return true when click on "Yes"
          child:const Text('Yes',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset:Offset.zero
          )])),
        ),
      ],
    ),
  )??false; //if showDialouge had returned null, then return false
}
Future<Widget> showCustomPopup(context,{String msg=""}) async {
  return await showDialog( //show confirm dialogue
    //the return value will be from "Yes" or "No" options
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: appBarColor,
      title:const Text('Warning !',style: TextStyle(color: textColor)),
      content:Text(msg,style:const TextStyle(color: textColor)),
      actions:[
        TextButton(
          onPressed: () => Navigator.of(context).pop(false),
          //return true when click on "Yes"
          child:const Text('Ok',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset:Offset.zero
          )])),
        ),
      ],
    ),
  )??const SizedBox.shrink(); //if showDialouge had returned null, then return false
}
late var monthNameGBL=[];
late var dayNameGBL=[];
late var yearNameGBL=[];
 Future<Map<String,String>> validateDobGBL(String checkDate) async {
   late var checked;
   late DateTime check;
   late DateTime nowDate;
   late int yearCalculate;
  late DateFormat format;
  try
      {
        await null;
        format = DateFormat("dd.MM.yyyy");
        check = format.parseStrict(checkDate);
        nowDate=DateTime.now();
        yearCalculate=nowDate.difference(check).inDays;
        if(yearCalculate<4380) {
          checked = {'msg': "limit", 'date': check.toString()};
        }
        else{
          checked = {'msg': "ok", 'date': check.toString(),'timeZone':check.timeZoneName,'utc':check.timeZoneOffset.toString(),'playerDateTime':nowDate.toString()};
        }
        return checked;
      }
      catch(e)
    {
    checked={'msg':"no",'date':''};
    return checked;
    }
}
//late List<dynamic> globalPlayingLevelList;

Future<bool> showPopupPlaying(context,String freindFeetId,String playingCoins,String partName,String partLength) async {
   return await showDialog( //show confirm dialogue
    //the return value will be from "Yes" or "No" options
     context: context,
    builder: (context) => AlertDialog(
      backgroundColor: appBarColor,
      title:const Text('Confirmation !',style: TextStyle(color: textColor)),
      content:const Text('Please choose your Playing Role .',style: TextStyle(color: textColor)),
      actions:[
        TextButton(
          onPressed:() async
                  {
                    String msg="Your friend "+playerName+ " is requested you to play for "+partName+ " "+ partLength+ " for "+playingCoins+" coins as Chaser .";
                    String response=await signalrObjGBL.sendChallenge(freindFeetId,msg,playingCoins,"Runner");
                   if(response=="ok")
                   {
                     Navigator.of(context).pushReplacementNamed("/playwaiting",arguments:{});
                   }
                   else
                     {
                       showSnack("Your friend is offline .");
                       Navigator.of(context).pop(false);
                     }
                  },
          //return false when click on "NO"
          child:const Text('Runner',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset:Offset.zero
          )])),
        ),

        TextButton(
          onPressed:() async
          {
            String msg="Your friend "+playerName+ " is requested you to play for "+partName+ " "+ partLength+ " for "+playingCoins+" coins as Runner .";
            String response=await signalrObjGBL.sendChallenge(freindFeetId,msg,playingCoins,"Chaser");
            if(response=="ok")
            {
              Navigator.of(context).pushReplacementNamed("/playwaiting",arguments:{});
            }
            else
            {
              showSnack("Your friend is offline .");
              Navigator.of(context).pop(false);
            }
          },
          //return true when click on "Yes"
          child:const Text('Chaser',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset:Offset.zero
          )])),
        ),
      ],
    ),
  )??false; //if showDialouge had returned null, then return false
}
void showSnack(msg)
{
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 13.0
  );
}
late var signalrObjGBL;
 late var playerName="";
 Future<bool> showPopupChallengeAccept(context,String msgReceived,String coins,String playingRole,String friendFeetId) async {
  return await showDialog( //show confirm dialogue
    //the return value will be from "Yes" or "No" options
    context: context,
    builder: (context) => AlertDialog(
      backgroundColor: appBarColor,
      title:const Text('Alert !',style: TextStyle(color: textColor)),
      content:Text(msgReceived,style: const TextStyle(color: textColor)),
      actions:[
        TextButton(
          onPressed:() async
          {
            String response=await signalrObjGBL.letsChallenge(friendFeetId,coins,playingRole);
            if(response=="ok")
            {
              Navigator.of(context).pushReplacementNamed("/playwaiting",arguments:{});
            }
            else
            {
              showSnack("Your friend is offline .");
              Navigator.of(context).pop(false);
            }
          },
          //return false when click on "NO"
          child:const Text('Accept',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset:Offset.zero
          )])),
        ),

        TextButton(
          onPressed:() async
          {
            String response=await signalrObjGBL.letsChallenge(friendFeetId,coins,playingRole);
            if(response=="ok")
            {
              Navigator.of(context).pushReplacementNamed("/playwaiting",arguments:{});
            }
            else
            {
              showSnack("Your friend is offline .");
              Navigator.of(context).pop(false);
            }
          },
          //return true when click on "Yes"
          child:const Text('Decline',style: TextStyle(color: textColor,fontWeight: fontWeight,fontSize: fontSizeApp,shadows:[Shadow(
              color: Colors.grey,
              blurRadius: 10.0,
              offset:Offset.zero
          )])),
        ),
      ],
    ),
  )??false; //if showDialouge had returned null, then return false
}



