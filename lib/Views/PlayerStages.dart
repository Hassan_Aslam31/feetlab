import 'package:flutter/material.dart';
import 'package:feetlab/Models/PlayerStagesModel.dart';
import 'package:feetlab/Views/layout.dart';

class PlayerStages extends StatefulWidget {
  @override
  _PlayerStagesState createState() => _PlayerStagesState();
}
class _PlayerStagesState extends State<PlayerStages> {
  late Future<List<PlayerStagesModelClass>> futureData;
  late String levelName;
  late dynamic routedValues;

  @override
  void initState()
  {
    super.initState();
  }
  @override
  void didChangeDependencies() {
    routedValues=ModalRoute.of(context)?.settings.arguments;
   levelName=routedValues['levelName'];
   futureData=getPlayerStagesList(routedValues['objectiveId']);
   super.didChangeDependencies();
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:() async{
        Navigator.pushReplacementNamed(context, '/Objectives');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title:Text(levelName),
              leading: TextButton.icon(
                onPressed: (){
                  Navigator.pushReplacementNamed(context, '/Objectives');
                },
                label:const Text(''),
                icon:const Icon(Icons.arrow_back_rounded,color:textColor)
              ),
              titleSpacing:0
            ),
            body:FutureBuilder<List<PlayerStagesModelClass>>(
              future:futureData,
              builder:(context,snapshot){
                if (snapshot.hasData) {
                  return PlayerStagesListBuilder(stagesList:snapshot.data!);
                } else if (snapshot.hasError) {
                  return mainServiceMSG;
                }
                return mainLoader;
              }
            )
        )
      )
    );
  }
}




