import 'package:feetlab/Views/layout.dart';
import 'package:flutter/material.dart';
import 'package:feetlab/Models/PlayerCoinsModel.dart';

class PlayerCoins extends StatefulWidget {
  @override
  _PlayerCoinsState createState() => _PlayerCoinsState();
}
class _PlayerCoinsState extends State<PlayerCoins> {
  late Future<PlayerCoinsModelClass> futureData;

  @override
  void initState()
  {
    super.initState();
    futureData=getPlayerCoins();
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:() async{
        Navigator.pushReplacementNamed(context, '/MainScreen');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title:const Text('Player Coins'),
              leading: TextButton.icon(
                onPressed: (){
                  Navigator.pushReplacementNamed(context, '/MainScreen');
                },
                label:const Text(''),
                icon:const Icon(Icons.arrow_back_rounded,color:textColor)
              ),
              titleSpacing:0
            ),
            body:FutureBuilder<PlayerCoinsModelClass>(
              future: futureData,
              builder: (context,snapshot)
              {
               if(snapshot.hasData)
                 {
                   return PlayerCoinsBuilder(playerCoins: snapshot.data!.playerCoins);
                 } else if(snapshot.hasError)
                   {
                     return mainServiceMSG;
                   }
               return mainLoader;
              }
            )
        )
      )
    );
  }
}
