import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:camerax/camerax.dart';
import 'dart:math';
class GamePlay extends StatefulWidget {
  @override
  _GamePlayState createState() => _GamePlayState();
}
class _GamePlayState extends State<GamePlay> {
  late MapboxMapController MapControllerMain;
  bool MyLocationEnabled=false;
  late CameraController CamViewController;
  late var UserCurrentLat=0.0;
  late var UserCurrentLong=0.0;
  late String PlayerRole;
  bool CameraOn=true;
  late Widget MainCameraView;
  late var LocVerticalAccuracy=0.0;
  late var LocSpeed=0.0;
  late var LocAltitude=0.0;
  late var LocBearing=0.0;
  late var LocHorizontalAccuracy=0.0;
  late var LocationHeadingAccuracy=0.0;
  late var LocationTrueHeading=0.0;
  late var LocationHeadingX=0.0;
  late var LocationHeadingY=0.0;
  late var LocationHeadingZ=0.0;
  late var LocationHeadingMegnatic=0.0;
  void OnMapCreated(MapboxMapController controllerMap)
  {
      MapControllerMain=controllerMap;
  }
  void MapStyleLoaded()
  {
     setState(() {
       MyLocationEnabled=true;
     });
  }
  void OnUserLocationUpdated(UserLocation userLocation)
  {
      UserCurrentLat=userLocation.position.latitude;
      UserCurrentLong=userLocation.position.longitude;
      LocationHeadingMegnatic=userLocation.heading!.magneticHeading!;
      LocationHeadingAccuracy=userLocation.heading!.headingAccuracy!;
      LocationTrueHeading=userLocation.heading!.trueHeading!;
      LocationHeadingX=userLocation.heading!.x!;
      LocationHeadingY=userLocation.heading!.y!;
      LocationHeadingZ=userLocation.heading!.z!;
      LocVerticalAccuracy=userLocation.verticalAccuracy!;
      LocSpeed=userLocation.speed!;
      LocAltitude=userLocation.altitude!;
      LocBearing=userLocation.bearing!;
      LocHorizontalAccuracy=userLocation.horizontalAccuracy!;


  }
  @override
  void initState()
  {
    super.initState();
    CamViewController=CameraController();
    PlayerRole="Runner";
    MainCameraView=CameraView(CamViewController);
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
        await CamViewController.startAsync();
    });
  }
  @override
  void dispose() {
   CamViewController.dispose();
   MapControllerMain.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:() async{
       // Navigator.pushReplacementNamed(context, '/playercoins');
        return true;
      },
      child: SafeArea(
        child: Scaffold(
          body:Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: [
                MainCameraView,
                ConstrainedBox(constraints:const BoxConstraints.expand(height:250),child:MapBoxBody(context)),
                 Align(
                  alignment: Alignment.bottomRight,
                  child:Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ElevatedButton(
                          style: TextButton.styleFrom(
                            backgroundColor: appBarColor,
                            shape:const CircleBorder(),
                          ),
                        onPressed: (){
                        MapControllerMain.animateCamera(CameraUpdate.newLatLngZoom(LatLng(UserCurrentLat, UserCurrentLong), 13));
                      },
                        child: const Icon(Icons.my_location,color: textColor)
                      ),
                      ElevatedButton(
                          style: TextButton.styleFrom(
                            backgroundColor: appBarColor,
                            shape:const CircleBorder(),
                          ),
                          onPressed: (){
                        MapControllerMain.animateCamera(CameraUpdate.zoomIn());
                      },  child: const Icon(Icons.zoom_in,color: textColor)
                      ),
                      ElevatedButton(
                          style: TextButton.styleFrom(
                            backgroundColor: appBarColor,
                            shape:const CircleBorder(),
                          ),
                          onPressed: (){
                        MapControllerMain.animateCamera(CameraUpdate.zoomOut());
                      },  child: const Icon(Icons.zoom_out,color: textColor)
                      ),
                    ],
                  ),
                 ),
                 Align(
                   alignment: Alignment.topLeft,
                   child: Padding(
                     padding: const EdgeInsets.all(5.0),
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         IconButton(
                           icon: ValueListenableBuilder(
                             valueListenable: CamViewController.torchState,
                             builder: (context, state, child) {
                               final color =
                               state == TorchState.off || CameraOn==false ? Colors.white : Colors.yellow;
                               return Icon(Icons.bolt, color: color);
                             },
                           ),
                           iconSize: 33.0,
                           onPressed: () => CameraOn==true?CamViewController.torch():showSnack("Torch cannot be opened in closed camera preview."),
                         ),
                         IconButton(
                           icon:Icon(Icons.camera_alt_outlined, color:CameraOn==true ? Colors.yellow : Colors.white),
                           iconSize: 33.0,
                           onPressed: () {
                             if(CameraOn)
                               {
                                 setState(() {
                                   CamViewController.dispose();
                                   CameraOn=false;
                                   MainCameraView=Container(color: Colors.black,);
                                 });
                               }
                             else
                               {
                                 setState(() {
                                   CamViewController=CameraController();
                                   MainCameraView=CameraView(CamViewController);
                                   CamViewController.startAsync();
                                   CameraOn=true;
                                 });
                               }
                           },
                         ),
                         Text('Vertical Accuracy : '+LocVerticalAccuracy.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),),
                         Text('Horizontal Accuracy : '+LocHorizontalAccuracy.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),),
                         Text('Altitude : '+LocAltitude.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),),
                         Text('Bearing : '+LocBearing.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),),
                         Text('Speed : '+LocSpeed.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),),
                         Text('LocMeg : '+LocationHeadingMegnatic.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),),
                         Text('LocHeadAcc : '+LocationHeadingAccuracy.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),),
                         Text('LocHeadTrue : '+LocationTrueHeading.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),),
                         Text('LocHeadX : '+LocationHeadingX.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),),
                         Text('LocHeadY : '+LocationHeadingY.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),),
                         Text('LocHeadZ : '+LocationHeadingZ.toString(),style:const TextStyle(color: Colors.white,fontSize:13.0),)
                       ],
                     ),
                   ),
                 ),
                 Align(
                  alignment: Alignment.bottomLeft,
                  child:Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children:  [
                       ElevatedButton(
                         style: TextButton.styleFrom(
                           backgroundColor: appBarColor,
                           shape:const CircleBorder(),
                         ),
                         child:Image.asset('assets/images/NorthIconMap.jpg',color:textColor,height: 27.0),
                     onPressed: (){
                       MapControllerMain.animateCamera(CameraUpdate.bearingTo(0.0));
                     },
                      ),
                      ElevatedButton(
                        style: TextButton.styleFrom(
                          backgroundColor: appBarColor,
                          shape:const CircleBorder(),
                        ),
                        child:Image.asset('assets/images/footprints.png',color:textColor,height: 31.0),
                        onPressed: (){

                        },
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).size.height * 0.3,
                  child:IconButton(
                    icon: const Icon(Icons.camera,color: Colors.white),
                    onPressed: () {  },
                    iconSize: 77.0,
                  ),
                )
              ],
            ),
        ),
      ),
    );
  }
  Widget MapBoxBody(BuildContext context)
  {
    return MapboxMap(
          initialCameraPosition:const CameraPosition(target: LatLng(0.0,0.0),zoom: 13.0),
          accessToken: 'pk.eyJ1IjoiaGFzc2FucmF6YWFzbGFtIiwiYSI6ImNsMjlqM2c5bTBqaGEzb3QzajFwNThwM3kifQ.BHrFvqsimqeFYC6L7pnVQw',
          onMapCreated: OnMapCreated,
          myLocationEnabled: MyLocationEnabled,
          trackCameraPosition: true,
          onStyleLoadedCallback:MapStyleLoaded,
        //  onUserLocationUpdated:OnUserLocationUpdated,
          myLocationTrackingMode:MyLocationTrackingMode.Tracking,
          logoViewMargins: const Point(-100, -100),
          attributionButtonMargins: const Point(-100, -100),
      );
  }
}




