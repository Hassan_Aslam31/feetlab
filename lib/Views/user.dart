import 'package:flutter/material.dart';
import 'package:feetlab/Views/layout.dart';
import 'package:feetlab/Models/usermodel.dart';
class userInfo extends StatefulWidget {
  late String userAction;
  userInfo({this.userAction=""});
  @override
  _userInfoState createState() => _userInfoState(userAction:userAction);
}
class _userInfoState extends State<userInfo> {
  late String userAction;
  late var routedValue;
 // late var buildContextAfter;
  _userInfoState({this.userAction=""});
  void GetDeviceInfo() async {
    try {
     final obj = userModelClass();
    var deviceInfo=await obj.getDeviceInfo();
      if(deviceInfo['id']=="unknown")
        {
          setState(() {
            mainLoader = const Center(
              child:Text('FeetLab can be playable only on Android and IOS phones .', style: TextStyle(fontSize: 21.0,),
              textAlign: TextAlign.center),
            );
          });
        }
      else
        {
          var response=await obj.checkUserProfile(deviceInfo['id']);
         //print(response);
          if(int.parse(response['responseCode'])==200)
            {
              var user=response['user'];
              if(user['userId']==0)
                {
                  Navigator.pushReplacementNamed(context,'/beawareMsg');
                }
              else {
                //setState(() {
                  feetId = user['userId'];
                  coinsGBL=user['playerCoins'];
                  playerName=user['playerName'];
                //});
                Navigator.pushReplacementNamed(context, '/mainscreen');
              }
            }
          else
            {
              setState(() {
                mainLoader = mainServiceMSG;
              });
            }
        }
    }
    catch(e)
    {
     // print(e);
      setState(() {
        mainLoader = mainServiceMSG;
      });
    }
  }

  void SaveDeviceInfo() async {
    try {
      final obj = userModelClass();
      var deviceInfo=await obj.getDeviceInfo();
      var response=await obj.saveUserProfile(deviceInfo['id'],deviceInfo['model'],deviceInfo['manufacturer'],routedValue['playerName'],routedValue['dob'],routedValue['timeZone'],routedValue['utc'],routedValue['playerDT']);
      if(int.parse(response['responseCode'])==200)
        {
          var user=response['user'];
           // setState(() {
              feetId = user['userId'];
              coinsGBL=user['playerCoins'];
           // });
            Navigator.pushReplacementNamed(context, '/mainscreen');
        }
        else
        {
          setState(() {
            mainLoader = mainServiceMSG;
          });
        }
    }
    catch(e)
    {
    //  print(e);
      setState(() {
        mainLoader = mainServiceMSG;
      });
    }
  }
  @override
  void initState()
  {
    super.initState();
    mainLoader=mainLoaderBack;
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      routedValue=ModalRoute.of(context)?.settings.arguments;
      if(userAction=="check")
      {
        GetDeviceInfo();
      }
      else
      {
        SaveDeviceInfo();
      }
    });
  }
  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
   // buildContextAfter=context;
   // routedValue=ModalRoute.of(context)?.settings.arguments;
    return WillPopScope(
      onWillPop: () async {
        return showExitPopup(context);
      },
      child: SafeArea(
        child: Scaffold(
            body:mainLoader
        ),
      ),
    );
  }
}
